# MARS_cansat


communication format from station to cansat:

"@xxxx"

a = rpi off
b = rpi on
c = camera power off
d = camera power on
e = sensors power off 
f = sensors power on
x = system check    #[xxx]_[GioD0 on/off]_[GioD1 on/off]_[GioD2 on/off]


Turn on rpi:        @b
Turn off rpi:       @a
Turn on camera:     @d
Turn off camera:    @c
Turn on sensors:    @f
Turn off sensors:   @e

