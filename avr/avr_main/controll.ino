void controll(char data[62]) {
  if (data[0] == '@' || data[6] == '@') {
    for (byte i = 1; i < 8; i++) {
      switch (data[i]) {
        case 'a':
          digitalWrite(RPI_P, LOW);
          break;
        case 'b':
          digitalWrite(RPI_P, HIGH);
          break;
        case 'c':
          digitalWrite(CAM_P, LOW);
          break;
        case 'd':
          digitalWrite(CAM_P, HIGH);
          break;
        case 'e':
          digitalWrite(SENSOR_P, LOW);
          break;
        case 'f':
          digitalWrite(SENSOR_P, HIGH);
          break;
      }
    }
  }
}