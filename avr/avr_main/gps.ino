void gps_check() {
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;

  // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;) {
    while (ss.available()) {
      char c = ss.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c))  // Did a new valid sentence come in?
        newData = true;
    }
  }

  if (newData) {
    float flat, flon;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
    GPS_lat = (flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat);
    GPS_lon = (flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon);
    GPS_alt = (gps.f_altitude() == TinyGPS::GPS_INVALID_F_ALTITUDE ? 0.0 : gps.f_altitude());
    GPS_speed = (gps.f_speed_kmph());
    GPS_HDOP = (gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
    GPS_sat = (gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    DEBUG("LAT=");
    DEBUG(GPS_lat);
    DEBUG(" LON=");
    DEBUG(GPS_lon);
    DEBUG(" ALT=");
    DEBUG(GPS_alt);
    DEBUG(" Speed=");
    DEBUG(GPS_speed);
    DEBUG(" SAT=");
    DEBUG(GPS_sat);
    DEBUG(" PREC=");
    DEBUG(GPS_HDOP);
  }

  gps.stats(&chars, &sentences, &failed);
  /*DEBUG(" CHARS=");
  DEBUG(chars);
  DEBUG(" SENTENCES=");
  DEBUG(sentences);
  DEBUG(" CSUM ERR=");
  DEBUGln(failed);
  //if (chars == 0)
  //  DEBUGln("** No data GPS");*/
}