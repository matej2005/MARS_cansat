
/*      DEBUG         */
#define DEBUG_EN true
#ifdef DEBUG_EN
#define SERIAL_BAUD 9600
#define DEBUGbegin() Serial.begin(SERIAL_BAUD)
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGbegin()
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

#include <RFM69.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>


#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 2     // My node ID (0 to 255)
#define TONODEID 1   // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define FREQUENCYSPECIFIC 433500000  // Should be value in Hz, now 433 Mhz will be set
#define ENCRYPT false                // Set to "true" to use encryption
#define ENCRYPTKEY "T0n3uh0dn3s"     // Use the same 16-byte key on all nodes
#define USEACK false                 // Request ACKs or not
#define DEBUG_EN true
#define UARTbaud 9600

/*      PINOUT        */
#define UART_RX 5
#define UART_TX 6
#define GPS_RX 9  //0u >  GPS-TX
#define GPS_TX 14  //1u >  GPS-RX
#define RF_CS 16
#define RF_IRQ 2
#define CAM_P 7
#define RPI_P 8
#define SENSOR_P 9
#define RPI_status 4

RFM69 radio(RF_CS,RF_IRQ);                            //spi
SoftwareSerial ss(GPS_RX, GPS_TX);      //uart
SoftwareSerial uart(UART_RX, UART_TX);  //uart
TinyGPS gps;          //serial
float GPS_lat, GPS_lon, GPS_alt, GPS_speed;
int GPS_HDOP, GPS_sat;
bool isRadioOk = true;
void setup() {
  delay(1000);
  Serial.begin(SERIAL_BAUD);
  Serial.println("Starting...");
  //DEBUGbegin();
  //DEBUGln("Starting...");
  uart.begin(UARTbaud);
  if (!radio.initialize(FREQUENCY, MYNODEID, NETWORKID)) {
    isRadioOk = false;
    Serial.println("RFM69HW initialization failed!");
  } else {
    radio.setFrequency(FREQUENCYSPECIFIC);
    radio.setHighPower(true);  // Always use this for RFM69HW
  }
  //power control
  pinMode(CAM_P, OUTPUT);
  digitalWrite(CAM_P, LOW);
  pinMode(SENSOR_P, OUTPUT);
  digitalWrite(SENSOR_P, LOW);
  pinMode(RPI_P, OUTPUT);
  digitalWrite(RPI_P, LOW);

  if (ENCRYPT) {
    radio.encrypt(ENCRYPTKEY);
  }

  RADIOsend("Cansat(MARS) station started", 30);
}

void loop() {
  //DEBUGln("ok");
  static char sendbuffer[62];
  static int sendlength = 0;
  // SENDING
  if (uart.available() > 0) {
    char input = uart.read();

    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    // If the input is a carriage return, or the buffer is full:
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      // Send the packet!
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  if (Serial.available() > 0) {
    char input = Serial.read();
    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      controll(sendbuffer);
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
    // Print out the information:

    DEBUG("received from node ");
    DEBUG(radio.SENDERID);
    DEBUG(": [");

    for (byte i = 0; i < radio.DATALEN; i++) {
      DEBUG((char)radio.DATA[i]);
    }
    DEBUG("], RSSI ");
    DEBUGln(radio.RSSI);
    controll(radio.DATA);

    if (radio.ACKRequested()) {
      radio.sendACK();
      DEBUGln("ACK sent");
    }
  }
}
