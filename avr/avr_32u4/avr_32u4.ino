#define VERSION "Beta 1.4"

//#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>
#include <RFM69.h>
#include <SPI.h>

/*      DEBUG         */
#define DEBUG_EN
#ifdef DEBUG_EN
#define SERIAL_BAUD 115200
#define DEBUGbegin() Serial.begin(SERIAL_BAUD)
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGbegin()
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

/*      RF            */
#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 3     // My node ID (0 to 255)
#define TONODEID 255     // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define FREQUENCYSPECIFIC 433500000  // Should be value in Hz, now 433 Mhz will be set
#define ENCRYPT false                // Set to "true" to use encryption
#define ENCRYPTKEY ""                // Use the same 16-byte key on all nodes
#define USEACK false                 // Request ACKs or not


#define UARTbaud 9600

/*      PINOUT        */
//#define UART_RX 1  //komunikace s rpi
//#define UART_TX 0
#define RF_CS 10  //cs RFM69
#define RF_IRQ 3  //přerušení z RFM69 DIO0
//GPIO
#define GPIOD0 7  //Camera power
#define GPIOD1 8  //RPI power
#define GPIOD2 9  //Sensors power

#define GPIOA0 A0
#define GPIOA1 A1
#define GPIOA2 A2

#define CAM_P GPIOD0
#define RPI_P GPIOD1
#define SENSOR_P GPIOD2


RFM69 radio(RF_CS, RF_IRQ, true);  //spi
//SoftwareSerial uart(UART_RX, UART_TX);  //uart
//AltSoftSerial uart(UART_RX, UART_TX);
//SoftwareSerial camerauart(CAMERARX,CAMERATX);

bool ok[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };  //kontrola funkcí   0-systém celý ok; 1-avr; 2-uart/rpi; 3-posledí ACK; 4; 5-RFM;6;7;8
bool gpioD[3] = { 0, 1, 0 };              //status GPIO Camera,RPI,Sensors
bool gpioA[3] = { 0, 0, 0 };
static char sendbuffer[256];  //62
static int sendlength = 0;

static char buffer[62];
static int bufferlenght = 0;
bool bufferFull = false;
bool lock = false;
void setup() {
  //nastaveni pinnu
  pinMode(GPIOD0, OUTPUT);
  pinMode(GPIOD1, OUTPUT);
  pinMode(GPIOD2, OUTPUT);

  digitalWrite(GPIOD0, LOW);   //camera
  digitalWrite(GPIOD1, HIGH);  //rpi
  digitalWrite(GPIOD2, LOW);   //sens
  delay(1000);

  DEBUGbegin();                             //begin debug USB
  Serial1.begin(UARTbaud);                  //gegin uart pro kameru
  DEBUG("Starting_32u4_cansat_MARS...\n");  //startovaci sprava do DEBUG

  if (!radio.initialize(FREQUENCY, MYNODEID, NETWORKID)) {  //kontrola radia
    DEBUG("RFM69HW initialization failed!\n");
    ok[5] = 0;
  } else {
    radio.setFrequency(FREQUENCYSPECIFIC);
    radio.setHighPower(true);  // Always use this for RFM69HW
    ok[5] = 1;
  }



  if (ENCRYPT) {  //sifrovani?
    radio.encrypt(ENCRYPTKEY);
  }

  DEBUG("Cansat(MARS) started [Node ");
  DEBUGHEX(MYNODEID, DEC);
  DEBUG("] ready ver: ");
  DEBUGln(VERSION);
  RADIOsend("start", 5);  //startovaci sprava na stanici
}

void loop() {

  // SENDING
  if (Serial1.available() > 0) {  //posilani z UART(RPI)
    char input = Serial1.read();

    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 256))  //61 CR or buffer full
    {
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  if (Serial.available() > 0) {  //posilani z DEBUG
    char input = Serial.read();
    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      controll(sendbuffer);  //ovladani pres DEBUG
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
#ifdef DEBUG_EN  //Debug bufferu
    DEBUG("\n! Received[");
    DEBUGHEX(radio.SENDERID, DEC);
    DEBUG("] RSSI: ");
    DEBUG(radio.RSSI);
    DEBUG(" : ");
    for (byte i = 0; i < radio.DATALEN; i++) {
      DEBUG((char)radio.DATA[i]);
    }
#endif
    controll(radio.DATA);  //ovladani ze stanice

    if (radio.ACKRequested()) {  //kdyz prijde ACK ozvi se
      radio.sendACK();
      //DEBUG("ACK\n");
      ok[3] = 1;
    }
  }
  RADIObuffer();
}