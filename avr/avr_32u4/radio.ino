void RADIOsend(const char sendbuffer[256], int sendlength) {

  if (lock) {
    DEBUG("added to buffer");
    bufferlenght = sendlength;
    for (byte i = 0; i < sendlength; i++) {
      buffer[i] = sendbuffer[i];
    }
    bufferFull = true;
  } else {
    lock = true;
  }

#ifdef DEBUG_EN  //Debug bufferu
  DEBUG("sending to node ");
  DEBUG(TONODEID);
  DEBUG(": [");
  for (byte i = 0; i < sendlength; i++) {
    DEBUG(sendbuffer[i]);
    //Serial1.print(sendbuffer[i]);
  }
  DEBUG("]  ");
  //Serial1.print("\n");
#endif
  if (USEACK) {  //bude se posilat ACK?
    if (radio.sendWithRetry(TONODEID, sendbuffer, sendlength))
      DEBUG(" ACK");
    else
      DEBUG(" no ACK");
  } else  // don't use ACK
  {
    //DEBUG("Sending without ACK");
    radio.send(TONODEID, sendbuffer, sendlength);
  }
  DEBUG("    Send ok\n");
  lock = false;
}
void RADIObuffer() {
  if (lock == false && bufferFull) {
    lock = true;
#ifdef DEBUG_EN  //Debug bufferu
    DEBUG("sending buffer");
    DEBUG("[");
    for (byte i = 0; i < sendlength; i++) {
      DEBUG(sendbuffer[i]);
    }
    DEBUG("]  ");
#endif
    RADIOsend(buffer, bufferlenght);
    lock = false;
    bufferFull = false;
  }
}
