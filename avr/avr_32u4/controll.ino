void controll(char data[62]) {
  if (data[0] == '@' || data[6] == '@') {
    for (byte i = 1; i < 8; i++) {
      switch (data[i]) {
        case 'a':
          digitalWrite(RPI_P, LOW);
          gpioD[1] = 0;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("GP1:off", 11);
          break;
        case 'b':
          digitalWrite(RPI_P, HIGH);
          gpioD[1] = 1;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("$b GP1:on", 10);
          
          break;
        case 'c':
        
          digitalWrite(CAM_P, LOW);
          gpioD[0] = 0;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("$c GP0:off", 11);
          
          break;
        case 'd':
          digitalWrite(CAM_P, HIGH);
          gpioD[0] = 1;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("$d GP0:on", 10);
          
          break;
        case 'e':
          
          digitalWrite(SENSOR_P, LOW);
          gpioD[2] = 0;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("$e GP2:off", 11);
          
          break;
        case 'f':
          digitalWrite(SENSOR_P, HIGH);
          gpioD[2] = 1;
          check();
          RADIOsend(sendbuffer, sendlength);
          //RADIOsend("$f GP:on", 10);
          
          break;
        case 'x':
          check();
          RADIOsend(sendbuffer, sendlength);
          break;
      }
    }
  }
}
