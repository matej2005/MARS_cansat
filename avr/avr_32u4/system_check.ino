int check() {//kontrola systemu
  DEBUG("\nSystem check DEC: ");
  int decimal = 0;
  int power = 1;

  for (int i = 7; i >= 0; i--) {
    if (ok[i]) {
      decimal += power;
    }
    power *= 2;
  }
  DEBUGln(decimal);

  sprintf(sendbuffer, "#x%.3d_%d_%d_%d", decimal,gpioD[0],gpioD[1],gpioD[2]);//priprava stringu checku
  sendlength=11;//nastaveni delky bufferu
  return decimal;
}