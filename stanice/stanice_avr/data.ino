int getdata() {
  if (radio.DATA[0] == '#') {
    //DEBUG("getdata: ");
    for (byte i = 1; i < radio.DATALEN; i++) {
      //DEBUG((char)radio.DATA[i]);
      switch (radio.DATA[i]) {
        case 'x':
          getstatus(i);
          break;
      }
    }
    //DEBUGln("");
    return (1);
  }
  return (0);
}
int getint(int i) {
  int y = 0;
  char out[10];
  i++;
  while ((radio.DATA[i] != '_') && !(i >= radio.DATALEN)) {
    out[y] = radio.DATA[i];
    y++;
    i++;
  }
  return (atoi(out));
}
float getfloat(int i) {
  int y = 0;
  char out[10];
  i++;
  while ((radio.DATA[i] != '_') && !(i >= radio.DATALEN)) {
    //DEBUG(radio.DATA[i]);
    out[y] = radio.DATA[i];
    y++;
    i++;
  }
  return (atof(out));
}
void getstatus(int i) {
  int y = 0;
  char out[3];
  i++;
  while ((radio.DATA[i] != '_') && !(i >= radio.DATALEN)) {
    out[y] = radio.DATA[i];
    y++;
    i++;
  }
  status = atoi(out);
  i++;
  //i++;
  gpioD[0] = (radio.DATA[i] == '1' ? 1 : 0);
  i++;
  i++;  
  gpioD[1] = (radio.DATA[i] == '1' ? 1 : 0);
  i++;
  i++;
  gpioD[2] = (radio.DATA[i] == '1' ? 1 : 0);
}