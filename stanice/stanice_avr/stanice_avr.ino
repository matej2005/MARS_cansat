

#define VERSION "Beta 1.3"
/*      DEBUG         */
#define DEBUG_EN true

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

#include <RFM69.h>
#include <SPI.h>
#include <LiquidCrystal.h>


#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 1     // My node ID (0 to 255)
#define TONODEID 3     // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define FREQUENCYSPECIFIC 433500000    // Should be value in Hz, now 433 Mhz will be set
#define ENCRYPT false                  // Set to "true" to use encryption
#define ENCRYPTKEY "TOPSECRETPASSWRD"  // Use the same 16-byte key on all nodes
#define USEACK false                   // Request ACKs or not

//RF
#define chip_select_pin 10
#define interupt_pin 2
//GPS


RFM69 radio(chip_select_pin, interupt_pin, true);
int status;
bool isRadioOk = true;
bool gpioD[3] = { 0, 0, 0 };

void setup() {
  delay(100);
  //Serial.println("Starting_avr_stanice_MARS...");
  // Initialize the RFM69HCW:

  if (!radio.initialize(FREQUENCY, MYNODEID, NETWORKID)) {
    isRadioOk = false;
    Serial.println("RFM69HW initialization failed!");
  } else {
    radio.setFrequency(FREQUENCYSPECIFIC);
    radio.setHighPower(true);  // Always use this for RFM69HW
  }
  // Turn on encryption if desired:
  if (ENCRYPT)
    radio.encrypt(ENCRYPTKEY);
  delay(1000);
  Serial.begin(SERIAL_BAUD);
  Serial.print("Stanice MARS [Node ");
  Serial.print(MYNODEID, DEC);
  Serial.print("] ready ver: ");
  Serial.println(VERSION);
}

void loop() {
  static char sendbuffer[62];
  static char data[62];
  static int sendlength = 0;


  // SENDING
  if (Serial.available() > 0) {
    char input = Serial.read();
    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      RADIOsend(sendbuffer, sendlength);
      sendlength = 0;  // reset the packet
    }
  }

  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
    //Serial.print("! Received[");
    //Serial.print(radio.SENDERID, DEC);
    Serial.print("!RSSI: ");
    Serial.print(radio.RSSI);
    Serial.print(" : ");
    for (byte i = 0; i < radio.DATALEN; i++) {
      Serial.print((char)radio.DATA[i]);
    }
    Serial.print("\n");

    if (radio.ACKRequested()) {
      radio.sendACK();
      //DEBUG("ACK");
    }
    /*if (getdata() == 1) {
      delay(100);
      Serial.print("----------------------------------\n");
      Serial.print("Camera: ");
      Serial.print(gpioD[0]);
      Serial.print(" Rpi: ");
      Serial.print(gpioD[1]);
      Serial.print(" Sens: ");
      Serial.print(gpioD[2]);
      Serial.print(" Status: ");
      Serial.println(status);
    }*/
  }
}