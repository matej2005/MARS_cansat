void RADIOsend(const char sendbuffer[62], int sendlength) {
  //static int sendlength = sizeof(sendbuffer);
  if (isRadioOk) {
#ifdef DEBUG_EN
    DEBUG("sending to node ");
    DEBUG(TONODEID);
    DEBUG(": [");
    for (byte i = 0; i < sendlength; i++) {
      DEBUG(sendbuffer[i]);
    }
    DEBUGln("]");
#endif

    if (USEACK) {
      if (radio.sendWithRetry(TONODEID, sendbuffer, sendlength))
        DEBUG("ACK received!");
      else
        DEBUG("no ACK received :(");
    } else  // don't use ACK
    {
      DEBUG("Sending without ACK");
      radio.send(TONODEID, sendbuffer, sendlength);
    }
    DEBUGln("    Send ok");
  }
}
//[A;305;0:0:0.0;998.44;32.58235;26.71;6;5;84;4.71]
//[B;305;0.0;0;0;0.0;0;0.0]


//;305;0:0:0.0;998.44;32.58235;26.71;6;5;84;4.71]
//;305;0.0;0;0;0.0;0;0.0]
int getdata() {
  uint8_t i = 0;
  if (!(radio.DATA[i] == 'A' || radio.DATA[i] == 'B' || radio.DATA[i] == 'C' || radio.DATA[i] == 'D')) {
    DEBUG("E: wrong data\n");
  }
  i++;
  if (!radio.DATA[i] == ';') DEBUG("E: wrong data\n");
  i++;
  if (radio.DATA[i] == 'A') {
    id = getint(&i);//id
    DEBUG("\n id: ");
    DEBUG(id);
    skip(&i);//timestamp
    pressure = getfloat(&i);//pressure
    DEBUG(" pressure: ");
    DEBUG(id);


  } else if (radio.DATA[i] == 'B') {
  }






  //DEBUG("getdata: ");
  /*for (byte i = 1; i < radio.DATALEN; i++) {
      DEBUG((char)radio.DATA[i]);
      switch (radio.DATA[i]) {
        case 'a':
          flat = (getfloat(i));
          break;
        case 'b':
          flon = (getfloat(i));
          break;
        case 'c':
          falt = (getfloat(i));
          break;
        case 'd':
          speed = (getfloat(i));
          break;
        case 'e':
          sat = (getint(i));
          break;
        case 'f':
          HDOP = (getint(i));
          break;
        case 't':
          temp = (getint(i));
          break;
        case 'h':
          humidity = (getint(i));
          break;
      }
    }
    //DEBUGln("");
    if (screenNum == 1) {
      drawHome();
    }
    if (screenNum == 0) {
      drawInfo();
    }
  }*/
  return (0);
}
int getint(uint8_t* i) {
  int y = 0;
  char out[10];
  *i++;
  while ((radio.DATA[*i] != ';') && !(*i >= radio.DATALEN)) {
    out[y] = radio.DATA[*i];
    y++;
    *i++;
  }
  return (atoi(out));
}
void skip(uint8_t* i) {
  *i++;
  while ((radio.DATA[*i] != ';') && !(*i >= radio.DATALEN)) {
    *i++;
  }
}
float getfloat(uint8_t* i) {
  int y = 0;
  char out[10];
  *i++;
  while ((radio.DATA[*i] != '_') && !(*i >= radio.DATALEN)) {
    //DEBUG(radio.DATA[i]);
    out[y] = radio.DATA[*i];
    y++;
    *i++;
  }
  return (atof(out));
}