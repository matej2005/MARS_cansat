void drawHome() {
  ucg.clearScreen();
  ucg.setFont(ucg_font_inr16_mr);
  ucg.setColor(52.5, 52.5, 52.5);
  ucg.setPrintPos(80, 20);
  ucg.print("MARS");

  //ucg.setFont(ucg_font_micro_mf);
  ucg.setFont(ucg_font_5x7_tf);
  ucg.setColor(74, 100, 108);
  ucg.setPrintPos(10, 20);
  ucg.print("Lat: ");
  ucg.print(flat);
  ucg.setPrintPos(10, 30);
  ucg.print("Lon: ");
  ucg.print(flon);
  ucg.setPrintPos(10, 40);
  ucg.print("Alt: ");
  ucg.print(falt);
  ucg.print("m");
  ucg.setPrintPos(10, 50);
  ucg.print("Speed: ");
  ucg.print(speed);
  ucg.print("km/h");

  ucg.setPrintPos(10, 70);
  ucg.print("Temp: ");
  ucg.print(temp);
  ucg.print("˚C");
  ucg.setPrintPos(10, 80);
  ucg.print("Hum: ");
  ucg.print(humidity);
  ucg.print("%");
  ucg.setPrintPos(10, 90);
  ucg.print("Pres: ");
  ucg.print(pressure);
  ucg.print("Hpa");
  //ucg.setFont(ucg_font_5x7_tf);
  ucg.setPrintPos(10, 120);
  ucg.print(gps.getHour());
  ucg.print(":");
  ucg.print(gps.getMinute());
  ucg.print("  ");
  ucg.print(gps.getDay());
  ucg.print("/");
  ucg.print(gps.getMonth());
  ucg.print("/");
  ucg.print(gps.getYear());
  //ucg.println(" HH:MM DD/MM/YYYY");
}
void drawInfo() {
  ucg.clearScreen();
  ucg.setFont(ucg_font_5x7_tf);
  ucg.setPrintPos(10, 20);
  ucg.print("Bat: 0v");
  ucg.setPrintPos(10, 30);
  ucg.print("Up time: 0s");
  ucg.setPrintPos(10, 40);
  ucg.print("ID: ");
  ucg.print(id);
  ucg.setPrintPos(10, 50);
  ucg.print("Rx: ");
  ucg.setPrintPos(10, 110);
  ucg.print("Ver: ");
  ucg.print(VERSION);
  
  bool l;
  for (byte i = 0; i < radio.DATALEN; i++) {
    if ((i < 20) && (!l)) {
      l = true;
      ucg.setPrintPos(10, 60);
    }
    ucg.print((char)radio.DATA[i]);
  }
}
void drawControll(uint8_t update) {
  if (update == 0) {
    ucg.clearScreen();
    ucg.setFont(ucg_font_5x7_tf);
    ucg.setPrintPos(10, 10);
    ucg.print(" On                 Off");
    drawButton(10, 20, 0, 0);   //cam on
    drawButton(100, 20, 0, 0);  //cam off
    drawButton(10, 45, 0, 0);   //sen on
    drawButton(100, 45, 0, 0);  //sen off
    drawButton(10, 70, 0, 0);   //rpi on
    drawButton(100, 70, 0, 0);  //rpi off
    drawButton(50, 110, 0, 0);   //test
  } else {
    //drawSwitch(10, 10, cam_p, (selectNum == 1), (update == 1));
    drawButton(10, 20, selectNum, 1);
    drawButton(100, 20, selectNum, 2);
    //drawSwitch(10, 35, sen_p, (selectNum == 2), (update == 2));
    drawButton(10, 45, selectNum, 3);
    drawButton(100, 45, selectNum, 4);
    //drawSwitch(10, 60, rpi_p, (selectNum == 3), (update == 3));
    drawButton(10, 70, selectNum, 5);
    drawButton(100, 70, selectNum, 6);

    drawButton(50, 110, selectNum, 7);
  }
  drawLeftArrow(10, 110, (selectNum == max_buttons_gui));
  drawstatusall(update);
}
void drawstatusall(uint8_t update) {
  ucg.setFont(ucg_font_inr16_mr);
  if (update == 1 || update == 0) {
    drawstatus(40, 18, cam_p, (update == 1));
    ucg.setColor(74, 100, 108);
    ucg.setPrintPos(45, 35);
    ucg.print("Cam");
  }
  if (update == 2 || update == 0) {
    drawstatus(40, 43, sen_p, (update == 2));
    ucg.setColor(74, 100, 108);
    ucg.setPrintPos(45, 60);
    ucg.print("Sen");
  }
  if (update == 3 || update == 0) {
    drawstatus(40, 68, rpi_p, (update == 3));
    ucg.setColor(74, 100, 108);
    ucg.setPrintPos(45, 85);
    ucg.print("Rpi");
  }
  if (update == 0) {
    ucg.setColor(74, 100, 108);
    ucg.setPrintPos(70, 110);
    ucg.print("test");
  }
}
void drawstatus(int x, int y, bool status, bool update) {
  if (status) {  //on
    ucg.setColor(0, 255, 0);
  } else {  //off
    ucg.setColor(255, 0, 0);
  }
  ucg.drawBox(x, y, 50, 20);
}
void drawSwitch(int x, int y, bool status, uint8_t select, bool update) {
  ucg.setColor(0, 0, 0);
  if (update) {
    ucg.drawBox(x - 1, y - 1, 41, 21);
  }
  if (select) ucg.setColor(0, 0, 255);
  else ucg.setColor(28, 28, 28);

  ucg.drawBox(x - 1, y - 1, 45, 25);
  if (status) {
    ucg.setColor(0, 0, 0);
    ucg.drawBox(x + 25, y, 15, 20);
    ucg.setColor(108, 108, 108);
    ucg.drawBox(10, y, 25, 20);
    ucg.setColor(255, 0, 0);
    ucg.setPrintPos(x + 2, y + 17);
    ucg.setFont(ucg_font_inr16_mr);
    ucg.print("ON");
  } else {
    ucg.setColor(0, 0, 0);
    ucg.drawBox(x, y, 15, 20);
    ucg.setColor(108, 108, 108);
    ucg.drawBox(x + 15, y, 25, 20);
    ucg.setColor(255, 0, 0);
    ucg.setPrintPos(x + 2, y + 17);
    ucg.setFont(ucg_font_inr16_mr);
    ucg.print("OFF");
  }
}
void drawButton(int x, int y, uint8_t select, int a) {
  //ucg.setColor(0, 0, 0);
  if ((select == a) || (select == a + 1) || (select == a - 1) || (select == max_buttons_gui)) {
    if (select == a && a != 0) ucg.setColor(0, 0, 255);
    else ucg.setColor(0, 0, 0);
    ucg.drawBox(x - 2, y - 2, 24, 24);

    ucg.setColor(0, 0, 100);
    ucg.drawBox(x, y, 20, 20);
  }
}
void drawLeftArrow(int x, int y, bool select) {
  if (select) ucg.setColor(0, 0, 255);
  else ucg.setColor(108, 108, 108);
  ucg.drawBox(x, y, 8, 5);
  ucg.drawTriangle(x, y - 5, x, y + 8, x - 6, y + 2);
}