#define VERSION "2.2 beta"



/*      DEBUG         */
#define DEBUG_EN

#ifdef DEBUG_EN
#define SERIAL_BAUD 115200
#define DEBUG(input) Serial.print(input)
#define DEBUGln(input) Serial.println(input)
#define DEBUGHEX(input, param) Serial.print(input, param)
#define DEBUGFlush() Serial.flush()
#else
#define DEBUG(input)
#define DEBUGln(input)
#define DEBUGHEX(input, param)
#define DEBUGFlush() ;
#endif

#include <OcsStorage.h>
#include <RFM69.h>
#include <Ucglib.h>
//#include "Time.h"
#include "Open_Cansat_GPS.h"

//#ifndef UCG_INTERRUPT_SAFE
//#define UCG_INTERRUPT_SAFE
//#endif

#define NETWORKID 122  // Must be the same for all nodes (0 to 255)
#define MYNODEID 4     // My node ID (0 to 255)
#define TONODEID 255   // Destination node ID (0 to 254, 255 = broadcast)
#define FREQUENCY RF69_433MHZ
#define FREQUENCYSPECIFIC 433500000    // Should be value in Hz, now 433 Mhz will be set
#define ENCRYPT false                  // Set to "true" to use encryption
#define ENCRYPTKEY "TOPSECRETPASSWRD"  // Use the same 16-byte key on all nodes
#define USEACK false                   // Request ACKs or not
bool isRadioOk = true;


#define Serial SerialUSB


/*      PINOUT        */
// BUTTONS
#define BUTTON_1 5
#define BUTTON_2 4
#define BUTTON_3 3
//RF
#define chip_select_pin 43
#define interupt_pin A4
//GPS
#define max_buttons_gui 8

RFM69 radio(chip_select_pin, interupt_pin, true);
//Adafruit_GC9A01A tft(TFT_CS, TFT_DC);
int temp, humidity, speed, age, alt2, humidity2, temp2, pressure, sat, HDOP, id = 0;
Ucglib_ST7735_18x128x160_HWSPI ucg(6, 7, -1);
OpenCansatGPS gps;

float flat, flon, falt, pres;
uint8_t screenNum = 1;
uint8_t selectNum = 1;
bool cam_p, sen_p, rpi_p;
void setup() {
  delay(1000);
  // Buttons
  pinMode(BUTTON_1, INPUT);
  pinMode(BUTTON_2, INPUT);
  pinMode(BUTTON_3, INPUT);
  pinMode(interupt_pin, INPUT);
  pinMode(9, INPUT);
  gps.begin();
  // Space behind font is transparent
  ucg.begin(UCG_FONT_MODE_TRANSPARENT);

  ucg.clearScreen();
  ucg.setRotate90();


  Serial.begin(SERIAL_BAUD);
  Serial.print("Node ");
  Serial.print(MYNODEID, DEC);
  Serial.print(" ready ver: ");
  Serial.println(VERSION);

  // Initialize the RFM69HCW:

  if (!radio.initialize(FREQUENCY, MYNODEID, NETWORKID)) {
    isRadioOk = false;
    Serial.println("RFM69HW initialization failed!");
  } else {
    radio.setFrequency(FREQUENCYSPECIFIC);
    radio.setHighPower(true);  // Always use this for RFM69HW
  }
  // Turn on encryption if desired:

  if (ENCRYPT)
    radio.encrypt(ENCRYPTKEY);
  delay(1000);
  drawHome();
}

void loop() {
  static char sendbuffer[62];
  static char data[62];
  static int sendlength = 0;

  buttons(digitalRead(BUTTON_1), digitalRead(BUTTON_2), digitalRead(BUTTON_3));
  // SENDING
  if (Serial.available() > 0) {
    char input = Serial.read();
    if (input != '\r')  // not a carriage return
    {
      sendbuffer[sendlength] = input;
      sendlength++;
    }
    if ((input == '\r') || (sendlength == 61))  // CR or buffer full
    {
      RADIOsend(sendbuffer, sendlength);
      DEBUGln("");
      if (screenNum == 1) {
        drawHome();
      }
      if (screenNum == 0) {
        drawInfo();
      }
      sendlength = 0;  // reset the packet
    }
  }
  // RECEIVING
  if (radio.receiveDone())  // Got one!
  {
    //id++;
    /*digitalWrite(23u,HIGH);
    delay(100);
    digitalWrite(23u,LOW);*/
    Serial.print("received from node ");
    Serial.print(radio.SENDERID, DEC);
    Serial.print(": [");
    for (byte i = 0; i < radio.DATALEN; i++) {
      Serial.print((char)radio.DATA[i]);
    }
    Serial.print("], RSSI ");
    Serial.println(radio.RSSI);


    if (radio.ACKRequested()) {
      radio.sendACK();
      Serial.println("ACK sent");
    }
    //decode_data();
    //if (radio.DATA[0] == '#') {

    getdata();

    //delay(100);
    /*Serial.print("Lat: ");
    Serial.println(flat);
    Serial.print("lon: ");
    Serial.println(flon);
    Serial.print("Alt: ");
    Serial.println(falt);
    Serial.print("Speed: ");
    Serial.println(speed);
    Serial.print("Temp: ");
    Serial.println(temp);
    Serial.print("Hum: ");
    Serial.println(humidity);*/

    //}
  }
}