void buttons(int button1, int button2, int button3) {
  if (button1 == LOW) {
    if (screenNum == 1) {
      screenNum = 0;
      drawInfo();
      delay(300);
      DEBUGln("drawInfo");
    } else if (screenNum == 2) {
      if (selectNum == 1) {
        selectNum = max_buttons_gui;
      } else {
        selectNum--;
      }
      drawControll(selectNum);
      delay(300);
      DEBUGln("");
      DEBUG("Select:");
      DEBUGln(selectNum);
    }
  } else if (button2 == LOW) {
    DEBUGln("Button 2 :control");
    DEBUGln("");
    DEBUG("Select:");
    DEBUGln(selectNum);
    if (screenNum == 2) {


      switch (selectNum) {
        case 1:
          radio.send(TONODEID, "@d", 2);  //camera on
          cam_p = 1;
          drawControll(1);
          break;
        case 2:
          radio.send(TONODEID, "@c", 2);  //camera off
          cam_p = 0;
          drawControll(1);
          break;
        case 3:
          radio.send(TONODEID, "@f", 2);  //sensors on
          sen_p = 1;
          drawControll(2);
          break;
        case 4:
          radio.send(TONODEID, "@e", 2);  //sensors off
          sen_p = 0;
          drawControll(2);
          break;
        case 5:
          radio.send(TONODEID, "@b", 2);  //rpi on
          rpi_p = 1;
          drawControll(3);
          break;
        case 6:
          radio.send(TONODEID, "@a", 2);  //rpi off
          rpi_p = 0;
          drawControll(3);
          break;
        case 7:
          getdata();
          
          break;
      }





      /*
      if (selectNum == 1) {
        if (cam_p == 0) {
          cam_p = 1;
          radio.send(TONODEID, "@d", 2);
        } else {
          cam_p = 0;
          radio.send(TONODEID, "@c", 2);
        }
      } else if (selectNum == 2) {
        if (sen_p == 0) {
          sen_p = 1;
          radio.send(TONODEID, "@f", 2);
        } else {
          sen_p = 0;
          radio.send(TONODEID, "@e", 2);
        }
      } else if (selectNum == 3) {
        if (rpi_p == 0) {
          rpi_p = 1;
          radio.send(TONODEID, "@b", 2);
        } else {
          rpi_p = 0;
          radio.send(TONODEID, "@a", 2);
        }
      }*/
      if (selectNum == max_buttons_gui) {
        selectNum = max_buttons_gui;
        screenNum = 1;
        drawHome();
      }
    }
  } else if (button3 == LOW) {
    if (screenNum == 1) {
      screenNum = 2;
      drawControll(0);
      delay(300);
      DEBUGln("Controll screen");
    } else if (screenNum == 0) {
      screenNum = 1;
      drawHome();
      delay(300);
      DEBUGln("Home");
    } else if (screenNum == 2) {
      if (selectNum == max_buttons_gui) {
        selectNum = 1;
      } else {
        selectNum++;
      }
      drawControll(selectNum);
      delay(300);
      DEBUG("\nSelect:");
      DEBUGln(selectNum);
    }
  }
}